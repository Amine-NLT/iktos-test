provider "aws" {
 access_key = ""
 secret_key = ""
 region = "eu-west-3"
}

resource "aws_security_group" "instance" {
  name = "iktos-test-security-group"
    egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_instance" "web" {
 ami = "${lookup(var.amis,var.region)}"
 count = "${var.nb}"
 key_name = "${var.key_name}"
 vpc_security_group_ids = ["${aws_security_group.instance.id}"]
 source_dest_check = false
 instance_type = "t2.micro"
tags = {
 Name = "${format("web-%03d", count.index + 1)}"
 }
}

resource "aws_security_group" "elb-sg" {
  name = "iktos-test-elb"
  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_elb" "elb" {
  name = "terraform-elb"
  security_groups = ["${aws_security_group.elb-sg.id}"]
  availability_zones = ["eu-west-3a", "eu-west-3b", "eu-west-3c"]
  instances= "${aws_instance.web.*.id}"
  health_check {
    healthy_threshold = 2
    unhealthy_threshold = 2
    timeout = 3
    interval = 30
    target = "HTTP:80/"
  }
  listener {
    lb_port = 80
    lb_protocol = "http"
    instance_port = "80"
    instance_protocol = "http"
  }
}
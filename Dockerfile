FROM python:3.7.1

LABEL Author="Amine Mohamadi"
LABEL E-mail="amine.mohamadi@outlook.com"

ENV FLASK_APP "app/app.py"
ENV FLASK_ENV "development"
ENV FLASK_DEBUG True

RUN mkdir /app
WORKDIR /app

COPY . /app/

RUN pip install --upgrade pip && \
    pip install pipenv && \
    pipenv install --dev --system --deploy --ignore-pipfile

EXPOSE 5000

CMD flask run 
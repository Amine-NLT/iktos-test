variable "nb" {
 default = 2
 }
variable "region" {
 description = "AWS region for hosting our your network"
 default = "eu-west-3"
}
variable "public_key_path" {
 description = "Enter the path to the SSH Public Key to add to AWS."
 default = "../terraform.pem"
}
variable "key_name" {
 description = "Key name for SSHing into EC2"
 default = "terraform"
}
variable "amis" {
 description = "Base AMI to launch the instances"
 default = {
 eu-west-3 = "ami-096b8af6e7e8fb927"
 }
}
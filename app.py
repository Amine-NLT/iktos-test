from flask import Flask
import pandas as pd

app = Flask(__name__)

data = pd.read_csv('adult.data')

print(data.head())

@app.route('/')
def index():
  return data.to_json()

@app.route('/row/<int:rowId>')
def getRow(rowId):
  return data.iloc[rowId].to_json()

@app.route('/column/<int:colId>')
def getColumn(colId):
  return data.iloc[:,colId].to_json()

@app.route('/head')
def say_hello():
  return data.head().to_json()

if __name__ == '__main__':
    app.run(host='0.0.0.0')
